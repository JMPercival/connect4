import Ember from 'ember';

export default Ember.Component.extend({
  playing: false,
  winner: undefined,
  draw: false,

  didInsertElement: function() {
      var stage = new createjs.Stage(this.$('#stage')[0]);
      //draw thh game board
      var board = new createjs.Shape();
      var graphics = board.graphics;
      graphics.beginFill('black');
      graphics.drawRect(0, 0, 350, 2);
      graphics.drawRect(0, 50, 350, 2);
      graphics.drawRect(0, 100, 350, 2);
      graphics.drawRect(0, 150, 350, 2);
      graphics.drawRect(0, 200, 350, 2);
      graphics.drawRect(0, 250, 350, 2);
      graphics.drawRect(0, 300, 350, 2);
      graphics.drawRect(0, 300, 350, 2);

      graphics.drawRect(0, 0, 2, 300);
      graphics.drawRect(50, 0, 2, 300);
      graphics.drawRect(100, 0, 2, 300);
      graphics.drawRect(150, 0, 2, 300);
      graphics.drawRect(200, 0, 2, 300);
      graphics.drawRect(250, 0, 2, 300);
      graphics.drawRect(300, 0, 2, 300);
      graphics.drawRect(350, 0, 2, 300);

      //gutters
      board.x = 40;
      board.y = 40;
      stage.addChild(board);
      //Create Markers
      var markers = {
          'x': [],
          'o': []
      };
      //creates 21 red counters and 21 red to fill the 6 x 7 table
      for (var x = 0; x < 21; x++) {
          var yellowMarker = new createjs.Shape();
          graphics = yellowMarker.graphics;
          graphics.beginStroke('orange');
          graphics.beginFill('yellow');
          graphics.setStrokeStyle(5);
          graphics.drawCircle(0, 0, 15);
          yellowMarker.visible = false; //un mark in game mode
          stage.addChild(yellowMarker);
          markers.x.push(yellowMarker);
          // red counter settings
          var redMarker = new createjs.Shape();
          graphics = redMarker.graphics;
          graphics.beginStroke('darkRed');
          graphics.beginFill('red');
          graphics.setStrokeStyle(5);
          graphics.drawCircle(0, 0, 15);
          redMarker.visible = false; //un mark in game mode
          stage.addChild(redMarker);
          markers.o.push(redMarker);
      }
      // update drawing
      this.set('markers', markers);
      this.set('stage', stage);
      stage.update();
  },
  //Click event handler
  click: function(ev) {
      //sets the positioning of the counter to be central of were the user clicks
      if(this.get('playing') && !this.get('winner')) {
          if (ev.target.tagName.toLowerCase() == 'canvas' && ev.offsetX >= 40 && ev.offsetY >= 40 && ev.offsetX < 400 && ev.offsetY < 340) {
              var x = Math.floor((ev.offsetX - 40) / 50);
              var y = Math.floor((ev.offsetY - 40) / 50);
              var state = this.get('state');
              //Drop (enables the counter to drop to next available position in the grid)
              var y = 5;
              while (state[x][y] == 'x' || state[x][y] == 'o'){
                y = y - 1;
            //console.log(y);
            }
              if (!state[x][y] & y >= 0) {
                  var player = this.get('player');
                  state[x][y] = player;

                  var move_count = this.get('moves')[player];
                  var marker = this.get('markers')[player][move_count];
                  marker.visible = true;
                  if (player == 'x') {
                    marker.x = 65 + x * 50;
                    marker.y = 65 + y * 50;
                  } else {
                    marker.x = 65 + x * 50;
                    marker.y = 65 + y * 50;
                  }
                  //checks the winner by incrementing a count on every players move
                  this.check_winner();
                  this.get('moves')[player] = move_count + 1;
                  if(player == 'x') {
                      this.set('player', 'o');
                  } else {
                      this.set('player', 'x');
                  }
                  this.get('stage').update();
              }
          }
      }
  },
  check_winner: function(){
      var patterns =[
        // checks every possible winning outcome, by iterating over the arrays and checking if these positions have been filled
        [[3, 0],[2, 1],[1, 2],[0, 3]],
        [[4, 0],[3, 1],[2, 2],[1, 3]],
        [[3, 1],[2, 2],[1, 3],[0, 4]],
        [[3, 0],[2, 1],[1, 2],[0, 3]],
        [[5, 0],[4, 1],[3, 2],[2, 3]],
        [[4, 1],[3, 2],[2, 3],[1, 4]],
        [[0, 5],[1, 4],[2, 3],[3, 2]],
        [[6, 0],[5, 1],[4, 2],[3, 3]],
        [[5, 1],[4, 2],[3, 3],[2, 1]],
        [[4, 2],[3, 3],[2, 4],[1, 5]],
        [[6, 1],[5, 2],[4, 3],[3, 4]],
        [[5, 2],[4, 3],[3, 4],[2, 5]],
        [[6, 2],[5, 3],[4, 4],[3, 5]],
        //Check right diagonal wins
        // [[0, 0],[1, 0],[2, 0],[3, 0]],
        [[0, 2],[1, 3],[2, 4],[3, 5]],
        [[0, 1],[1, 2],[2, 3],[3, 4]],
        [[1, 2],[2, 3],[3, 4],[4, 5]],
        [[0, 0],[1, 1],[2, 2],[3, 3]],
        [[1, 1],[2, 2],[3, 3],[4, 4]],
        [[2, 2],[3, 3],[4, 4],[5, 5]],
        [[1, 0],[2, 1],[3, 2],[4, 3]],
        [[2, 1],[3, 2],[4, 3],[5, 4]],
        [[3, 2],[4, 3],[5, 4],[6, 4]],
        [[2, 0],[3, 1],[4, 2],[5, 3]],
        [[3, 1],[4, 2],[5, 3],[6, 4]],
        [[3, 0],[4, 1],[5, 2],[6, 3]],
        // Check all horizontal wins
        [[0, 0],[0, 1],[0, 2],[0, 3]],
        [[0, 1],[0, 2],[0, 3],[0, 4]],
        [[0, 2],[0, 3],[0, 4],[0, 5]],
        [[1, 0],[1, 1],[1, 2],[1, 3]],
        [[1, 1],[1, 2],[1, 3],[1, 4]],
        [[1, 2],[1, 3],[1, 4],[1, 5]],
        [[2, 0],[2, 1],[2, 2],[2, 3]],
        [[2, 1],[2, 2],[2, 3],[2, 4]],
        [[2, 2],[2, 3],[2, 4],[2, 5]],
        [[3, 0],[3, 1],[3, 2],[3, 3]],
        [[3, 1],[3, 2],[3, 3],[3, 4]],
        [[3, 2],[3, 3],[3, 4],[3, 5]],
        [[4, 0],[4, 1],[4, 2],[4, 3]],
        [[4, 1],[4, 2],[4, 3],[4, 4]],
        [[4, 2],[4, 3],[4, 4],[4, 5]],
        [[5, 0],[5, 1],[5, 2],[5, 3]],
        [[5, 1],[5, 2],[5, 3],[5, 4]],
        [[5, 2],[5, 3],[5, 4],[5, 5]],
        [[6, 0],[6, 1],[6, 2],[6, 3]],
        [[6, 1],[6, 2],[6, 3],[6, 4]],
        [[6, 2],[6, 3],[6, 4],[6, 5]],
        // Check all vertical wins
        [[0, 0],[0, 1],[0, 2],[0, 3]],
        [[0, 0],[1, 0],[2, 0],[3, 0]],
        [[1, 0],[2, 0],[3, 0],[4, 0]],
        [[2, 0],[3, 0],[4, 0],[5, 0]],
        [[3, 0],[4, 0],[5, 0],[6, 0]],
        [[0, 1],[1, 1],[2, 1],[3, 1]],
        [[1, 1],[2, 1],[3, 1],[4, 1]],
        [[2, 1],[3, 1],[4, 1],[5, 1]],
        [[3, 1],[4, 1],[5, 1],[6, 1]],
        [[0, 2],[1, 2],[2, 2],[3, 2]],
        [[1, 2],[2, 2],[3, 2],[4, 2]],
        [[2, 2],[3, 2],[4, 2],[5, 2]],
        [[3, 2],[4, 2],[5, 2],[6, 2]],
        [[0, 3],[1, 3],[2, 3],[3, 3]],
        [[1, 3],[2, 3],[3, 3],[4, 3]],
        [[2, 3],[3, 3],[4, 3],[5, 3]],
        [[3, 3],[4, 3],[5, 3],[6, 3]],
        [[0, 4],[1, 4],[2, 4],[3, 4]],
        [[1, 4],[2, 4],[3, 4],[4, 4]],
        [[2, 4],[3, 4],[4, 4],[5, 4]],
        [[3, 4],[4, 4],[5, 4],[6, 4]],
        [[0, 5],[1, 5],[2, 5],[3, 5]],
        [[1, 5],[2, 5],[3, 5],[4, 5]],
        [[2, 5],[3, 5],[4, 5],[5, 5]],
        [[3, 5],[4, 5],[5, 5],[6, 5]],
        [[0, 6],[1, 6],[2, 6],[3, 6]],
        [[1, 6],[2, 6],[3, 6],[4, 6]],
        [[2, 6],[3, 6],[4, 6],[5, 6]],
        [[3, 6],[4, 6],[5, 6],[6, 6]]
      ];
//      checks the game state through looping tyhrpugh the above patterns and checking for a win or a draw
      var state = this.get('state');
      for(var pidx = 0; pidx < patterns.length; pidx++) {
          var pattern = patterns[pidx];
          var winner = state[pattern[0][0]][pattern[0][1]];
          if(winner) {
              for(var idx = 1; idx < pattern.length; idx++) {
                  if(winner != state[pattern[idx][0]][pattern[idx][1]]) {
                      winner = undefined;
                      break;
                  }
              }
              if(winner) {
                this.set('winner', winner);
                break;
              }
          }
      } //this keywords interacts with the hbs page so the players name is printed to the screen using the accompaning html page
      if(!this.get('winner')) {
          var draw = true;
          for(var x = 0; x <= 2; x++){
              for(var y = 0; y <= 2; y++){
                  if(!state[x][y]){
                      draw = false;
                      break;
                  }
              }
          }
          this.set('draw', draw);
      }
  },
  actions: {
      start: function() {
          this.set('playing', true);
          this.set('winner', undefined);
          this.set('draw', undefined);
          this.set('state', [ //sets the game up by declaring each grid as blank (undefined)
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined],
              [undefined, undefined, undefined, undefined, undefined, undefined]]);
          this.set('moves', {
              'x': 0,
              'o': 0
          });
          // using this keyword to interact with the players function above, calling it then counting each players limit of counters to declare the game a draw
          this.set('player', 'x');
          var markers = this.get('markers');
          for (var idx = 0; idx < 21; idx++) {
              markers.x[idx].visible = false;
              markers.o[idx].visible = false;
          }
          this.get('stage').update();
      }
  },


});
